<?php
namespace Festin;

class Cproxy
{
    private $proxies = array();
    private $prev_proxies = array();

    function __construct()
    {
    }

    function eat()
    {
        $url = "http://www.spys.ru/proxylist/";
        $curl = \curl_init($url);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $ht = curl_exec($curl);
        $pattern = "<form method='post' action='\/proxylist\/'>.+?(?#)<input type='hidden' name='xf0' value='([a-f0-9]+)'>.+?<\/form>";
        if (!preg_match("/$pattern/", $ht, $m)) {
            throw new \RangeException("There is no form data");
        }
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS,
            ['xpp' => 3,
                'xf0' => $m[1],
                'xf1' => 1,
                'xf2' => 0,
                'xf4' => 0]
        );
        $ht = curl_exec($curl);
        if (preg_match("/<script[^>]*>([0-9a-z=;^]+)<\/script>/", $ht, $m)) {
            $rawVars = $m[1];
            $rawVars = explode(';', $rawVars, 21);
            array_pop($rawVars);
            foreach ($rawVars as $v) {
                $KV = explode('=', $v);
                $aVars[$KV[0]] = $KV[1];
            }
            unset($m);
        } else {
            throw new \Exception("There is no data!");
        }
        if (preg_match_all("/([0-9.]+)<script[^>]*>document.write\(\"[^\"]+\"\+(.+?)\)<\/script>/", $ht, $m)) {
            $len = count($m[1]);
            for ($i = 0; $i < $len; $i++) {
                $ip = $m[1][$i];
                $rport = $m[2][$i];
                $port = '';
                $aInStrVars = preg_split("/[\(\)\+^]/", $rport);
                foreach ($aInStrVars as $v) {
                    if (!$v) continue;
                    $rport = str_replace($v, $aVars[$v], $rport);
                }
                if (preg_match_all("/\(([0-9^]+)\)\+?/", $rport, $mV)) {
                    foreach ($mV[1] as $rawn) {
                        $number = eval("return $rawn;");
                        $port .= $number;
                    }
                }
                $this->proxies[] = "$ip:$port";
            }
        } else {
            return -2;
        }
    }

    function getProxies()
    {
        return $this->proxies;
    }

    function getPCount()
    {
        return count($this->proxies);
    }

    function print_list()
    {
        foreach ($this->proxies as $el) {
            echo "$el\n";
        }
    }

    function getRequestCount()
    {
        return count($this->prev_proxies);
    }

    function getProxy()
    {
        do {
            $max = count($this->proxies) - 1;
            if ($max < 0) {
                break;
            }
            $r = array_rand($this->proxies);
            $proxy = $this->proxies[$r];
            $this->proxies[$r] = $this->proxies[$max];
            array_pop($this->proxies);
            try {
                $this->checkProxy($proxy);
                $this->prev_proxies[] = $proxy;
                return $proxy;
            } catch (\Exception $e) {
                // do nothing?
            }
        } while ($max > -1);
        throw new \RangeException('Proxies list is empty');
    }
    function checkProxy($proxy) {
        $tc = curl_init();
        $url = "http://www.google.com";
        curl_setopt($tc, CURLOPT_URL, $url);
        curl_setopt($tc, CURLOPT_RETURNTRANSFER, 1);
        // таймаут при использовании должен быть не меньше
        curl_setopt($tc, CURLOPT_TIMEOUT, 5);
        curl_setopt($tc, CURLOPT_PROXY, $proxy);
        curl_exec($tc);
        $info = curl_getinfo($tc, CURLINFO_HTTP_CODE);
        if (!in_array($info, [200, 301, 302])) {
            throw new \Exception();
        }
    }
}

?>
