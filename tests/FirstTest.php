<?php
use \Festin\Cproxy;

class FirstTest extends \PHPUnit_Framework_TestCase
{
	public function testEat() {
        $c = new Cproxy();
		$this->assertEquals(0, $c->getPCount());
		$c->eat();
		$this->assertNotEquals(0, $c->getPCount());
		return $c;
	}
	/**
	* @depends testEat
	*/
	public function testGetProxy($c) {
		$prev_proxies = array();
		$tc = curl_init();
		$url = "http://www.google.com";
		curl_setopt($tc, CURLOPT_URL, $url);
		//curl_setopt($tc, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($tc, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($tc, CURLOPT_TIMEOUT, 20);
		for($i=0;$i<100;$i++) {
			try {
				$proxy = $c->getProxy();
				$this->assertEquals($i+1, $c->getRequestCount());
				$this->assertRegExp("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,5}/", $proxy);
				$this->assertNotContains($proxy, $prev_proxies);
				curl_setopt($tc, CURLOPT_PROXY, $proxy);
				$res = curl_exec($tc);
				$info = curl_getinfo($tc, CURLINFO_HTTP_CODE);
                //file_put_contents("html/".parse_url($proxy, PHP_URL_HOST).".html", $info);
		        //file_put_contents("html/".parse_url($proxy, PHP_URL_HOST).".html", $res, FILE_APPEND);
				$prev_proxies[] = $proxy;
			} catch (RangeException $e) {
				// there is no proxies in list
				$this->assertEquals(0, $c->getPCount());
				break;
			}
		}
	}

}
